# My personal vim config

In this repo you have available my [`.vimrc`](.vimrc) config.

You need to install [vim-plug](https://github.com/junegunn/vim-plug)
as plugin manager.

Then run `:PlugInstall` to install all the plugins.

Then if everything is OK, you need to install all the coc-vim extensions you need,
you can find them in their [repo](https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions)
but first you need to install nodejs.
