set number
set confirm
set smartcase
set ignorecase
set hlsearch
set incsearch
set autowrite
set autoindent
set mouse=a
set numberwidth=1
set clipboard+=unnamedplus
set showcmd
set ruler
set encoding=utf-8
set showmatch
set sw=2
set relativenumber
set laststatus=2
" Open new split panes to right and below
set splitbelow
set splitright

set cursorline

"Folding
set foldmethod=indent
set foldlevel=1
" set foldclose=all
set nofoldenable
highlight LineNr ctermfg=grey
syntax enable
syntax on
set hidden
set path+=**
set suffixesadd+=.js

" command! -nargs=0 Prettier :CocCommand prettier.formatFile

call plug#begin('~/.vim/plugged')

" Themes 
Plug 'morhetz/gruvbox'

" IDE
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'raimondi/delimitmate'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-repeat'
Plug 'vim-scripts/loremipsum'
"Plug 'scrooloose/syntastic'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-commentary'
Plug 'ryanoasis/vim-devicons'
Plug 'mustache/vim-mustache-handlebars'

call plug#end()

" let g:coc_global_extensions = ['coc-emmet', 'coc-css', 'coc-html', 'coc-json', 'coc-prettier', 'coc-tsserver']

colorscheme gruvbox
let g:gruvbox_contrast_dark = "hard"

" Config for gitgutter
let g:gitgutter_sign_added = '✚'
let g:gitgutter_sign_modified = '✹'
let g:gitgutter_sign_removed = '-'
let g:gitgutter_sign_removed_first_line = '-'
let g:gitgutter_sign_modified_removed = '-'

" Config for nerdtreemap
let NERDTreeQuitOnOpen=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
" let NERDTreeIgnore = []
" let NERDTreeStatusline = ''
"let NERDTreeMapOpenInTab='<ENTER>'
"let NERDTreeMapActivateNode='v'

let g:mustache_abbrevations = 1

let mapleader=" "

nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>nn :NERDTreeFind<CR>

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>wq :wq<CR>
nmap <Leader>c :noh<CR>
nmap + ^
nmap <Leader>y "+y
nmap <Leader>Y "+Y
noremap <C-w>+ :resize +5<CR>
noremap <C-w>- :resize -5<CR>
noremap <C-w>< :vertical:resize +5<CR>
noremap <C-w>> :vertical:resize -5<CR>

" turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>
"start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
" open terminal on ctrl+n
function! OpenTerminal()
  split term://zsh
  resize 10
endfunction
nnoremap <c-n> :call OpenTerminal()<CR>


inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

